import numpy
from matplotlib import pyplot


def compute(x0, y0, X, n, f):
    h = (X - x0) / (n - 1)  # Step size

    x = numpy.linspace(x0, X, n)
    y = numpy.zeros([n])
    y[0] = y0
    ty = numpy.zeros([n])

    for i in range(1, n):
        ty[i] = y[i - 1] + h * f(x[i - 1], y[i - 1])
        y[i] = y[i - 1] + (h / 2) * (f(x[i - 1], y[i - 1]) + f(x[i], ty[i]))

    return x, y


if __name__ == "__main__":
    x0 = 0
    y0 = 1
    X = 12 / 5
    n = 100
    x, y = compute(x0, y0, X, n, lambda x, y: numpy.sin(x) + y)
    pyplot.plot(x, y, 'o')
    pyplot.xlabel("Value of x")
    pyplot.ylabel("Value of y")
    pyplot.title("Approximation Solution")

    pyplot.show()
