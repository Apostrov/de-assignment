import numpy
from matplotlib import pyplot
from matplotlib.widgets import TextBox

from EulerMethod import compute as eulerCompute
from ImprovedEulerMethod import compute as impEulerCompute
from RungeKuttaMethod import compute as rungeKuttaCompute

pyplot.switch_backend('QT4Agg')  # For fullscreen view

# Inital Value
x0 = 0
y0 = 1
X = 12 / 5
n = 256
f = lambda x, y: numpy.sin(x) + y
exactForm = lambda x, y: (-numpy.cos(x) - numpy.sin(x) + 3 * (numpy.e ** x)) / 2


def computeSolutions(x0, y0, X, n, f, exactForm):
    # Compute Exact
    xExact = numpy.linspace(x0, X, n)
    yExact = numpy.zeros([n])
    yExact[0] = y0

    for i in range(1, n):
        yExact[i] = exactForm(xExact[i - 1], yExact[i - 1])

    xEuler, yEuler = eulerCompute(x0, y0, X, n, f)
    xImpEuler, yImpEuler = impEulerCompute(x0, y0, X, n, f)
    xRunKut, yRunKut = rungeKuttaCompute(x0, y0, X, n, f)

    return xExact, yExact, yEuler, yImpEuler, yRunKut


def computeErrors(x, yExact, yEuler, yImpEuler, yRunKut):
    xErrors = x  # Because x - always the same
    yEulerErrors = numpy.zeros([n])
    yImpEulerErrors = numpy.zeros([n])
    yRunKutErrors = numpy.zeros([n])

    for i in range(1, n):
        yEulerErrors[i] = yExact[i] - yEuler[i]
        yImpEulerErrors[i] = yExact[i] - yImpEuler[i]
        yRunKutErrors[i] = yExact[i] - yRunKut[i]

    return xErrors, yEulerErrors, yImpEulerErrors, yRunKutErrors


def drawSolutions(x, yExact, yEuler, yImpEuler, yRunKut):
    solutionsPlt = pyplot.subplot(2, 1, 1)
    pyplot.subplots_adjust(top=0.85)
    solutionsPlt.plot(x, yEuler, 'ro-', label='Euler’s method')
    solutionsPlt.plot(x, yImpEuler, 'bo-', label='Improved Euler’s method')
    solutionsPlt.plot(x, yRunKut, 'go-', label='Runge-Kutta method')
    solutionsPlt.plot(x, yExact, 'yo-', label='Exact solution')
    pyplot.xlabel("Value of x")
    pyplot.ylabel("Value of y")
    pyplot.title("Approximation Solution")
    pyplot.grid(True)
    legend = solutionsPlt.legend(loc='upper left', shadow=True, fontsize='x-large')
    legend.get_frame().set_facecolor('C0')

    return solutionsPlt


def drawError(x, yEulerErrors, yImpEulerErrors, yRunKutErrors):
    xErrors = x  # Because x - always the same
    errorsPlt = pyplot.subplot(2, 1, 2)
    errorsPlt.plot(xErrors, yEulerErrors, 'ro-')
    errorsPlt.plot(xErrors, yImpEulerErrors, 'bo-')
    errorsPlt.plot(xErrors, yRunKutErrors, 'go-')
    pyplot.xlabel("Value of x")
    pyplot.ylabel("Value of errors")
    pyplot.grid(True)

    return errorsPlt


# Call draw and compute functions
xExact, yExact, yEuler, yImpEuler, yRunKut = computeSolutions(x0, y0, X, n, f, exactForm)
solutionsPlt = drawSolutions(xExact, yExact, yEuler, yImpEuler, yRunKut)
xErrors, yEulerErrors, yImpEulerErrors, yRunKutErrors = computeErrors(xExact, yExact, yEuler, yImpEuler, yRunKut)
errorsPlt = drawError(xErrors, yEulerErrors, yImpEulerErrors, yRunKutErrors)


def redraw(solutionsPlt, errorsPlt):
    xExact, yExact, yEuler, yImpEuler, yRunKut = computeSolutions(x0, y0, X, n, f, exactForm)
    solutionsPlt.clear()
    solutionsPlt = drawSolutions(xExact, yExact, yEuler, yImpEuler, yRunKut)

    xErrors, yEulerErrors, yImpEulerErrors, yRunKutErrors = computeErrors(xExact, yExact, yEuler, yImpEuler, yRunKut)
    errorsPlt.clear()
    errorsPlt = drawError(xErrors, yEulerErrors, yImpEulerErrors, yRunKutErrors)


def stepsSubmit(text):
    global n
    global solutionsPlt
    global errorsPlt
    n = int(text)
    redraw(solutionsPlt, errorsPlt)


# Draw Input Fields
stepBoxAxes = pyplot.axes([0.15, 0.9, 0.08, 0.05])
stepBox = TextBox(stepBoxAxes, 'Number of step', initial=str(n))
stepBox.on_submit(stepsSubmit)

# Make fullscreen
figManager = pyplot.get_current_fig_manager()
figManager.window.showMaximized()
pyplot.show()
